@extends('products.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="text-center">
            <h2><strong style="font-family:verdana;font-size:200%;">Apple Word</strong></h2>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif

        <div class="container">
            <button style="background-color:darkcyan" id="btn-product" class="btn btn-success btn-add" data-target="#add-product" data-toggle="modal">Add Product</button>
            <br><br>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr style="background-color:cadetblue">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th width="280px">Action</th>
                    </tr>
                    @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->price }}</td>
                        <td>
                            <button type="button" data-target="#show-product" data-toggle="modal" class="btn btn-info" id="btn-show" data-id={{ $product->id }}>Show</button>
                            <button data-url="#" ​ type="button" data-target="#edit-product" data-toggle="modal" class="btn btn-warning btn-edit">Edit</button>
                            <button data-url="#" ​ type="button" data-target="#delete-product" data-toggle="modal" class="btn btn-danger btn-delete">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

    {!! $products->links() !!}

    @endsection
