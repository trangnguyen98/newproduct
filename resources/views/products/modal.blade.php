<!--Modal create-->
<div id="add-product" class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ route('products.store') }}" id="form-add-product" method="POST">
				@csrf
				<div class="modal-header">
					<h4 class="modal-title" >Add product</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input name="name" type="text" class="form-control" placeholder="Name">
					</div>
					<div class="form-group">
						<label>Price</label>
						<input name="price" type="text" class="form-control" placeholder="Price">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Add</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--Modal edit-->
<div id="edit-product" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="{{ route('products.update', $product->id) }}" id="form-update-product" method="POST">
				@csrf
				<div class="modal-header">
					<h4 class="modal-title">Edit product</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input name="name" type="text" class="form-control" placeholder="Name">
					</div>
					<div class="form-group">
						<label>Price</label>
						<input name="price" type="text" class="form-control" placeholder="Price">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Edit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!--Modal show-->
<div id="show-product" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Show</h5>
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>ID: <span id="product-id"></span></p>
				<p>Name: <span id="product-name"></span></p>
				<p>Price: <span id="product-price"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<!--Modal delete--> 
<div class="modal fade" id="delete-product" tabindex="-1" role="dialog" aria-labelledby="deleteproductLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteproductLabel">Delete</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure want to delete?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button data-id={{ $product->id }} type="button" class="btn btn-primary">Delete</button>
      </div>
    </div>
  </div>
</div>